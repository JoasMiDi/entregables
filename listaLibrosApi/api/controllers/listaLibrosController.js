const repo = require('./listaLibrosRepo');
exports.list_all_books = (req, res) => {
    var data = repo.get();
    res.json(data);
};
exports.create_a_book = (req, res) => {
    var book = req.body;
    repo.add(book);
    res.json("{}");
};
exports.read_a_book = (req, res) => {
    var book = repo.find(req.params.bookId);
    res.json(book);
};
exports.update_a_book = (req, res) => {
    var book = req.body;
    repo.upd(book);
    res.json("{}");
};
exports.delete_a_book = (req, res) => {
    console.log(req.params.bookId);
    repo.del(req.params.bookId);
    res.json("{}");
};