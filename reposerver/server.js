const express = require('express'),
      cors = require('cors'),
  app = express(),
  port = process.env.PORT || 3392;

  app.use(express.urlencoded({ extended: true }))
  app.use(express.json());
  app.use(cors());
  const routes = require('./api/routers/listaLibrosRouter'); //importing route
  routes(app); //register the route
  app.get('*', (req, res)=>{
    res.status(404).send({url: req.originalUrl + ' not found'})
  })
  app.listen(port);
console.log('book list RESTful API server started on: ' + port);