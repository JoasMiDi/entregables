(function (exports){

    function greet(name){
        var options = { weekday:'long', year:'numeric', month:'long', day:'numeric'};
        var now =  new Date();
        var formattedData =  now.toLocaleString("es-MX", options);
        return `Hola, ${name}! Hoy es ${formattedData}`;
    }
    exports.greet = greet;
})(this);