const assert =  require('assert');
const greeter = require('../greeter.js');
const sinon = require('sinon');
describe("test de greeter", function(){
    it("check greet function", function(){
        var clock = sinon.useFakeTimers(new Date(2021,3,15));
        assert.strictEqual(greeter.greet("Enric"), "Hola, Enric! Hoy es jueves, 15 de abril de 2021");
    })
})