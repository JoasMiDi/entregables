const assert =  require('assert');
const unavez = require('../unavez.js');
const sinon = require('sinon');

it("llama a función original", function(){
    var callback = sinon.fake();
    var proxy = unavez.llamar(callback);
    var obj = {};

    proxy.call(obj, 1, 2, 3);
    proxy.call(obj, 1, 2, 4);

    assert(callback.calledOnce);
    assert(callback.calledWith(1, 2, 3));
})

it("comprobar retorno", function(){
    var callback = sinon.fake.returns(42);
    var proxy = unavez.llamar(callback);
    assert(proxy() === 42);
    assert(proxy() === 42);
})