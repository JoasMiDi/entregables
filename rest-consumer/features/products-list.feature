Feature: Lista Productos
    Scnenario: Cargar lista de productos
        When we request the products list
        Then we should recieve 
            | nombre | descripcion |
            | Móvil XL | Pantalla grande, una de las mejores |
            | Móvil mini | Una de las mejores cámaras |
            | Móvil Std | Ninguna característica especial |