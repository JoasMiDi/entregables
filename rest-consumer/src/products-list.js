import { LitElement, html, css } from 'lit-element';

class ProductsList  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        productos: {type:Array}
    };
  }

  constructor() {
    super();
    this.productos = [];
    this.productos.push({"nomre": "Móvil XL", "descripcion": "Pantalla grande, una de las mejores"});
    this.productos.push({"nomre": "Móvil mini", "descripcion": "Una de las mejores cámaras"});
    this.productos.push({"nomre": "Móvil Std", "descripcion": "Ninguna característica especial"});
  }

  render() {
    return html`
      <div class="contenedor">
        ${this.productos.map(p => html`
            <div class="producto">
                <h3>${p.nombre}</h3>
                <p>${p.descripcion}</p>
            </div>
        `)}
      </div>
    `;
  }

  createRenderRoot(){
      return this;
  }

}

customElements.define('products-list', ProductsList);