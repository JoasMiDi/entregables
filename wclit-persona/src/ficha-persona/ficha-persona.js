import { LitElement, html, css } from 'lit-element';
import { OrigenPersona } from '../origen-persona/origen-persona.js';

class FichaPersona  extends LitElement {

  static get styles() {
    return css`
      div {
        border: 1px solid;
        border-radius: 8px;
        padding: 10px;
        margin: 10px;
      }
    `;
  }

  static get properties() {
    return {
        nombre:{ type:String },
        apellidos:{ type:String },
        anios:{ type:Number },
        foto:{ type:Object },
        nivel:{ type:String },
        bg: {type:String}
    };
  }

  constructor() {
    super();
    this.nombre = "Pedro";
    this.apellidos = "Fernández Fernández";
    this.anios = "2";
    this.foto = {
        src: "./src/ficha-persona/img/persona.jpg",
        alt: "Foto Persona"
    }
    this.bg = "gray";
    //this.nivel = "";
  }

  render() {
    return html`
      <div style="width:400px; background-color:${this.bg}">
        <label for="inombre">Nombre</label>
        <input type="text" id="inombre" name="inombre" value="${this.nombre}" @input="${this.updateName}"></input>
        </br>
        <label for="iapellidos">Apellidos</label>
        <input type="text" id="iapellidos" name="iapellidos" value="${this.apellidos}"></input>
        </br>
        <label for="ianios">Años</label>
        <input type="number" id="ianios" name="ianios" value="${this.anios}" @input="${this.updateAnios}"></input>
        </br>
        <label for="inivel">Nivel</label>
        <input type="text" id="inivel" name="inivel" value="${this.nivel}" disabled></input>
        </br>
        <origen-persona  @origen-set="${this.origenChange}"></origen-persona>
        </br>
        <img src="${this.foto.src}" heigth="50px" width="50px" alt="${this.foto.alt}" ></img>
      </div>
    `;
  }

  updated(changedProperties){
    if(changedProperties.has("nombre")){
      console.log("Nombre anterior: " + changedProperties.get("nombre") + ". Nombre nuevo: " + " " + this.nombre) ;
    }

    if(changedProperties.has("anios")){
      this.actualizarNivel();
    }

  }

  updateName(e){
    this.nombre = e.target.value;
  }

  updateAnios(e){
    this.anios = e.target.value;
  }

  actualizarNivel(){
    if(this.anios >= 7){
      this.nivel = "Líder";
    }else if(this.anios >= 5){
        this.nivel = "Senior";
    }else if(this.anios >= 3){
        this.nivel = "Team";
    }else{
        this.nivel = "Junior";
    }
  }

  origenChange(e){
    var origen = e.detail.message;
    if (origen === "USA"){
      this.bg = "blue";
    }else if(origen === "Mexico"){
      this.bg = "green";
    }else if(origen === "España"){
      this.bg = "red";
    }else if(origen === "Colombia"){
      this.bg = "yellow";
    }
  }

}

customElements.define('ficha-persona', FichaPersona);